package abstracts;

public abstract class Calentador{

    protected String modelo;
    protected String capacidad;
    protected int tubos;

    public Calentador(String modelo, String capacidad, int tubos){
        this.modelo = modelo;
        this.capacidad = capacidad;
        this.tubos = tubos;

    }

    public abstract void mostrarCaracteristicas();


}
